
cd ./utils && unzip swig.zip && cd swig
echo ${PWD##*/}
if [ ${PWD##*/} == "swig" ] 
then
    find . -exec touch {} \;
    make clean
    ./configure --without-tcl

    if [ $? -eq 0 ]
    then
      # Result is OK! Just continue...
      echo "Configure swig... PASS!"
    else
      # houston we have a problem
      exit 1
    fi

    make

    if [ $? -eq 0 ]
    then
      # Result is OK! Just continue...
      echo "Make swig... PASS!"
    else
      # houston we have a problem
      exit 1
    fi

    make install

    if [ $? -eq 0 ]
    then
      # Result is OK! Just continue...
      echo "Make install swig... PASS!"
    else
      # houston we have a problem
      exit 1
    fi

    cd ..
    rm -r swig*
    cd ..
fi

rm -rf Papers

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/pip3 install setuptools==41.6.0
/opt/ANDRAX/PYENV/python3/bin/pip3 install -r modules/_requirements.txt

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Install module requirements... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/HomePWN

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX/HomePWN
chmod -R 755 /opt/ANDRAX/HomePWN

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
